<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интернет-магазин \"Одежда\"");
?><div class="container">
	<div class="row">
		<div class="col-sm-12 mr-main__slider">
			 <?$APPLICATION->IncludeComponent(
	"bisexpert:owlslider",
	"",
	Array(
		"AUTO_HEIGHT" => "Y",
		"AUTO_PLAY" => "Y",
		"AUTO_PLAY_SPEED" => "5000",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMPOSITE" => "Y",
		"COUNT" => "8",
		"DISABLE_LINK_DEV" => "Y",
		"DRAG_BEFORE_ANIM_FINISH" => "Y",
		"ENABLE_JQUERY" => "N",
		"ENABLE_OWL_CSS_AND_JS" => "Y",
		"HEIGHT_RESIZE" => "300",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "action",
		"IMAGE_CENTER" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"IS_PROPORTIONAL" => "N",
		"ITEMS_SCALE_UP" => "N",
		"LINK_URL_PROPERTY_ID" => "0",
		"MAIN_TYPE" => "iblock",
		"MOUSE_DRAG" => "Y",
		"NAVIGATION" => "Y",
		"NAVIGATION_TYPE" => "arrows",
		"PAGINATION" => "Y",
		"PAGINATION_NUMBERS" => "N",
		"PAGINATION_SPEED" => "800",
		"RANDOM" => "N",
		"RANDOM_TRANSITION" => "N",
		"RESPONSIVE" => "Y",
		"REWIND_SPEED" => "1000",
		"SCROLL_COUNT" => "1",
		"SECTION_ID" => "0",
		"SHOW_DESCRIPTION_BLOCK" => "N",
		"SLIDE_SPEED" => "200",
		"SORT_DIR_1" => "asc",
		"SORT_DIR_2" => "asc",
		"SORT_FIELD_1" => "id",
		"SORT_FIELD_2" => "",
		"SPECIAL_CODE" => "unic",
		"STOP_ON_HOVER" => "Y",
		"TEXT_PROPERTY_ID" => "0",
		"TOUCH_DRAG" => "Y",
		"TRANSITION_TYPE_FOR_ONE_ITEM" => "default",
		"WIDTH_RESIZE" => "1180"
	)
);?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4" style="text-align: center;">
			<div class="mr-banner--main-three">
				 <?$APPLICATION->IncludeComponent(
	"custom:london.smartbanner",
	".mr_top_three_banners",
	Array(
		"IBLOCK_ID" => "5",
		"IBLOCK_SECTION" => "17",
		"IBLOCK_TYPE" => "LONDON_SMART_BANNER"
	)
);?>
			</div>
		</div>
		<div class="col-sm-4" style="text-align: center;">
			<div class="mr-banner--main-three">
				 <?$APPLICATION->IncludeComponent(
	"custom:london.smartbanner",
	".mr_top_three_banners",
	Array(
		"IBLOCK_ID" => "5",
		"IBLOCK_SECTION" => "18",
		"IBLOCK_TYPE" => "LONDON_SMART_BANNER"
	)
);?>
			</div>
		</div>
		<div class="col-sm-4" style="text-align: center;">
			<div class="mr-banner--main-three">
				 <?$APPLICATION->IncludeComponent(
	"custom:london.smartbanner",
	".mr_top_three_banners",
	Array(
		"IBLOCK_ID" => "5",
		"IBLOCK_SECTION" => "19",
		"IBLOCK_TYPE" => "LONDON_SMART_BANNER"
	)
);?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="mr-rec-offer">
				 Категории
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"mr_catalog_category",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "Y",
		"HIDE_SECTION_NAME" => "N",
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array("ID","CODE","XML_ID","NAME","SORT","DESCRIPTION","PICTURE","DETAIL_PICTURE","IBLOCK_TYPE_ID","IBLOCK_ID","IBLOCK_CODE","IBLOCK_EXTERNAL_ID","DATE_CREATE","CREATED_BY","TIMESTAMP_X","MODIFIED_BY",""),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "/catalog/#SECTION_CODE#/",
		"SECTION_USER_FIELDS" => array("",""),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "TILE"
	)
);?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="mr-rec-offer">
				 Новинки
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.top", 
	".default", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPARE_PATH" => "",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "/catalog/#ID#",
		"DISPLAY_COMPARE" => "Y",
		"ELEMENT_COUNT" => "12",
		"ELEMENT_SORT_FIELD" => "shows",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "1c_catalog",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "5",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_LIMIT" => "5",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PROPERTY_CODE" => array(
			0 => "BLOG_POST_ID",
			1 => "CML2_ARTICLE",
			2 => "",
		),
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "/catalog/#SECTION_CODE#/",
		"SEF_MODE" => "Y",
		"SEF_RULE" => "",
		"SHOW_CLOSE_POPUP" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "site",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "SECTION",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="mr-rec-offer">
				 Наши рекомендации
			</div>




<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	".default",
	array(
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => "8",
		"BASKET_URL" => "/personal/cart/",
		"COMPONENT_TEMPLATE" => "",
		"IBLOCK_TYPE" => "1c_catalog",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "12",
		"LINE_ELEMENT_COUNT" => "4",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
			3 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "5",
		"TEMPLATE_THEME" => "site",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"LABEL_PROP" => "-",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"OFFERS_CART_PROPERTIES" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"ADD_TO_BASKET_ACTION" => "ADD",
		"PAGER_TEMPLATE" => "round",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

		</div>
	</div>
</div>
<div class="workarea">
	<div class="container">
		<div class="row">
			<div class="bx-content col-xs-12">

 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>