<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
    <div class="bx-hdr-profile">
<? if (!$compositeStub && $arParams['SHOW_AUTHOR'] == 'Y'): ?>
		<div class="bx-basket-block">
        <i class="fa fa-user"></i>
        <? if ($USER->IsAuthorized()):
            $name = trim($USER->GetFullName());
            if (!$name)
                $name = trim($USER->GetLogin());
            if (strlen($name) > 15)
                $name = substr($name, 0, 12) . '...';
            ?>
            <a href="<?= $arParams['PATH_TO_PROFILE'] ?>"><?= htmlspecialcharsbx($name) ?></a>
            &nbsp;
            <a href="?logout=yes"><?= GetMessage('TSB1_LOGOUT') ?></a>
        <? else: ?>
            <a href="<?= $arParams['PATH_TO_REGISTER'] ?>?login=yes"><?= GetMessage('TSB1_LOGIN') ?></a>
            &nbsp;
            <a href="<?= $arParams['PATH_TO_REGISTER'] ?>?register=yes"><?= GetMessage('TSB1_REGISTER') ?></a>
        <? endif ?>
    </div>
<? endif ?> 
    <div class="bx-basket-block"  style="padding: 0;">
<?php if (!$arResult["DISABLE_USE_BASKET"]): ?>


    <span class="mr-cart">
<i class="fa fa-shopping-cart mr-cart__i" aria-hidden="true"></i> <a
            href="<?= $arParams['PATH_TO_BASKET'] ?>"><?= GetMessage('TSB1_CART') ?></a>

        <?php if (!$compositeStub): ?>

            <?php if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')): ?>
                <lavel id="cart-badge"
                       class="badge badge-warning mr-cart__badge"><?= $arResult['NUM_PRODUCTS'] ?></lavel>
            <?php endif ?>

			    <?php if ($arParams['SHOW_TOTAL_PRICE'] == 'Y'): ?>
                <br <?php if ($arParams['POSITION_FIXED'] == 'Y'): ?>class="hidden-xs"<? endif ?>/>
				<p>
				<?= GetMessage('TSB1_TOTAL_PRICE') ?>
                        <?php if ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'): ?>
                    <strong><?= $arResult['TOTAL_PRICE'] ?></strong>
		</p>
                <?php endif ?>

            <?php endif; ?>
        <?php endif; ?>

</span>


    </div>
    </div>
<?php endif; ?>