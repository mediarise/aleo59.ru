<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>


<div class="mr-profile text-right">
    <? if (!$compositeStub && $arParams['SHOW_AUTHOR'] == 'Y'): ?>
        <div class="mr-profile__block">
            <? if ($USER->IsAuthorized()):
                $name = trim($USER->GetFullName());
                if (!$name)
                    $name = trim($USER->GetLogin());
if (strlen($name) > 35)
$name = substr($name, 0, 20) . '...';
                ?>

                <button type="button" class="mr-btn-user btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user"></i> <?= htmlspecialcharsbx($name) ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="<?= $arParams['PATH_TO_PROFILE'] ?>">Мой кабинет</a></li>
                    <li><a href="?logout=yes"><?= GetMessage('TSB1_LOGOUT') ?></a></li>

                </ul>
            <? else: ?>


                <button type="button" class="mr-btn-user btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user"></i> Личный кабинет
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">


                    <li><a href="<?= $arParams['PATH_TO_REGISTER'] ?>?login=yes"><?= GetMessage('TSB1_LOGIN') ?></a>
                    </li>
                    <li>
                        <a href="<?= $arParams['PATH_TO_REGISTER'] ?>?register=yes"><?= GetMessage('TSB1_REGISTER') ?></a>
                    </li>
                </ul>

            <? endif ?>
        </div>
    <? endif ?>
