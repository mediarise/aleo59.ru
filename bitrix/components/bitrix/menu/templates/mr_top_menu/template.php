<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="mr-nav-top">

	<ul class="mr-nav-top__list">
<?foreach($arResult as $arItem):?>

	<?if ($arItem["PERMISSION"] > "D"):?>
		<li class="mr-nav-top__item">

  <?if(isset($arItem["PARAMS"]["ICON_ICO"])):?>
<i class='mr-nav-top__ico fa <?=$arItem["PARAMS"]["ICON_ICO"]?>'></i>


    <?endif?>
			<a class="mr-nav-top__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>

<?endforeach?>

	</ul>
</nav>
<div class="menu-clear-left"></div>
<?endif?>