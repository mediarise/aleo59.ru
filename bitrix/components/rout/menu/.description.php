<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage("T_IBLOCK_DESC_MENU_ITEMS"),
	"DESCRIPTION" => Loc::getMessage("T_IBLOCK_DESC_MENU_ITEMS_DESC"),
	"ICON" => "/images/menu_ext.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "utility",
		"CHILD" => array(
			"ID" => "navigation",
			"NAME" => Loc::getMessage("MAIN_NAVIGATION_SERVICE")
		)
	),
);