(function($) {
	$.fn.equalHeights = function(minHeight, maxHeight) {
		tallest = (minHeight) ? minHeight : 0;
		this.each(function() {
			if($(this).height() > tallest) {
				tallest = $(this).height();
			}
		});
		if((maxHeight) && tallest > maxHeight) tallest = maxHeight;
		return this.each(function() {
			$(this).height(tallest);
		});
	}
})(jQuery);

BX.ready(function() {
	var menuDiv = document.body.querySelector("[data-role='x-menu']");
	if(!menuDiv)
		return;

	var sfEls = BX.findChild(menuDiv, { tagName: "li" }, true, true);
	for(var i = 0; i < sfEls.length; i++)
	{
		BX.bind(sfEls[i], 'mouseover', function() {
			BX.addClass(this, 'jshover');
			BX.addClass(BX.findChild(this, {tagName: "a"}, true, false), 'hover');
			BX.addClass(BX.findChild(this, {tagName: "span", className: "parent_span"}, true, false), 'over');
			BX.addClass(BX.findChild(this, {tagName: "ul"}, true, false), 'show_menu');

			if(typeof(jQuery) != 'undefined')
				$(this).parents('li.first_lvl_parent').find('ul').equalHeights();
		});

		BX.bind(sfEls[i], 'mouseout', function() {
			BX.removeClass(this, 'jshover');
			BX.removeClass(BX.findChild(this, {tagName: "a"}, true, false), 'hover');
			BX.removeClass(BX.findChild(this, {tagName: "span", className: "parent_span"}, true, false), 'over');
			BX.removeClass(BX.findChild(this, {tagName: "ul"}, true, false), 'show_menu');

			if(typeof(jQuery) != 'undefined')
				$(this).parents('li.first_lvl_parent').find('ul').equalHeights();
		});
	}
});