<?
IncludeModuleLangFile(__FILE__);
Class london_smartbanner extends CModule
{
	const MODULE_ID = 'london.smartbanner';
	var $MODULE_ID = 'london.smartbanner'; 
	var $IBLOCK_TYPE = 'LONDON_SMART_BANNER';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("london.smartbanner_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("london.smartbanner_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("london.smartbanner_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("london.smartbanner_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CLondonSmartbanner', 'OnBuildGlobalMenu');
		if (cModule::IncludeModule("iblock")){
			$iblockTYPE = $this->IBLOCK_TYPE;
			
			/* add IBLOCK_TYPE */
			$ibRes =  CIBlockType::GetList(array("SORT"=>"ASC"), array("ID"=>$iblockTYPE));
			if (!($iblockType = $ibRes->GetNext())){
				$ibType = new CIBlockType();
				$arFields = Array(
					'ID'=>$iblockTYPE,
					'SECTIONS'=>'Y',
					'IN_RSS'=>'N',
					'SORT'=>100,
					'LANG'=>Array(
						'ru'=>Array(
							'NAME'=>GetMessage("IBLOCK_TYPE_NAME"),
							'SECTION_NAME'=>GetMessage("IBLOCK_SECTION_NAME"),
							'ELEMENT_NAME'=>GetMessage("IBLOCK_ELEMENT_NAME")
							)
						)
					);
					$ibType->Add($arFields);
					$iblockAdd = true;
			}
			if (!$iblockAdd){
				$iblockCODE = $iblockTYPE."_IBLOCK";
				$ibRes = CIBlock::GetList(false, array("IBLOCK_TYPE"=>$iblockTYPE, "CODE"=>$iblockCODE));
				if ($iblock = $ibRes ->GetNext()) $iblockID = $iblock["ID"];
			}
			/* add IBLOCK */
			if (!$iblockID){
				$ib = new CIBlock();
				$arrSites = array();
				$objSites = CSite::GetList();
				while ($arrSite = $objSites->Fetch())
					$arrSites[] = $arrSite["ID"];
				$arFields = Array(
				  "ACTIVE" => "Y",
				  "NAME" => GetMessage("london.smartbanner_MODULE_NAME"),
				  "CODE" => $iblockCODE,
				  "LIST_PAGE_URL" => "" ,
				  "DETAIL_PAGE_URL" => "",
				  "IBLOCK_TYPE_ID" => $iblockTYPE,
				  "SITE_ID" => $arrSites,
				  "SORT" => 1000,
				  "PICTURE" => "",
				  "DESCRIPTION" => GetMessage("IBLOCK_DESCRIPTION"),
				  "DESCRIPTION_TYPE" => "TEXT",
				  "GROUP_ID" => Array("2"=>"R", "1"=>"R"),
				  "ELEMENT_NAME"=> GetMessage("ELEMENT_NAME"),
				  "ELEMENTS_NAME"=>GetMessage("ELEMENTS_NAME"),
				  "ELEMENT_ADD"=> GetMessage("ELEMENT_ADD"),
				  "ELEMENT_EDIT"=> GetMessage("ELEMENT_EDIT"),
				  "ELEMENT_DELETE"=>GetMessage("ELEMENT_DELETE"),
				  "SECTION_NAME"=>GetMessage("SECTION_NAME"),
				  "SECTION_ADD"=>GetMessage("SECTION_ADD"),
				  "SECTION_EDIT"=>GetMessage("SECTION_EDIT"),
				  "SECTION_DELETE"=>GetMessage("SECTION_DELETE")
				  );
				 $iblockID =  $ib->Add($arFields);
				 
				 $arProps = Array(
				 array(
					  "NAME" => GetMessage("PROPERTY_REF"),
					  "ACTIVE" => "Y",
					  "SORT" => "10",
					  "CODE" => "REF",
					  "PROPERTY_TYPE" => "S",
					  "IBLOCK_ID" => $iblockID),
				  array(
					  "NAME" => GetMessage("SHOW_IN_SUBSECTIONS"),
					  "ACTIVE" => "Y",
					  "SORT" => "20",
					  "CODE" => "SUBSECTION",
					  "PROPERTY_TYPE" => "L",
					  "LIST_TYPE"=>"C",
					  "IBLOCK_ID" => $iblockID,
					  "VALUES" =>array(
						Array(
						  "VALUE" => "Y",
						  "DEF" => "N",
						  "SORT" => "10"
						),
					  ),  
				  ),
					array(
					  "NAME" => GetMessage("ADRESS_WITH_BANNER"),
					  "ACTIVE" => "Y",
					  "SORT" => "30",
					  "MULTIPLE" => "Y",
					  "CODE" => "PAGE",
					  "PROPERTY_TYPE" => "S",
					  "IBLOCK_ID" => $iblockID,  
					),
					  array(
					  "NAME" => GetMessage("ADRESS_EXCLUSION"),
					  "ACTIVE" => "Y",
					  "SORT" => "40",
					  "MULTIPLE" => "Y",
					  "CODE" => "PAGE_EXCLUSION",
					  "PROPERTY_TYPE" => "S",
					  "IBLOCK_ID" => $iblockID,  
					),
				  );
					$ibp = new CIBlockProperty;
				  foreach ($arProps as $prop):
						$propID = $ibp->Add($prop);
						$arPropsIds[$prop["CODE"]] = $propID;
				  endforeach;
			
			/* view in list */
			$columns = "NAME,ACTIVE,PREVIEW_PICTURE,PREVIEW_TEXT,PROPERTY_$arPropsIds[REF],PROPERTY_$arPropsIds[PAGE],PROPERTY_$arPropsIds[SUBSECTION], PROPERTY_$arPropsIds[PAGE_EXCLUSION]";
				$option_hash = "tbl_iblock_list_".md5($iblockTYPE.".".$iblockID);
				$arOptions = array(
					 array(
						  'c' => 'list',
						  'n' => $option_hash,
						  'd' => 'Y',
						  'v' => array(
							   'columns' => $columns,
							   'by' => 'timestamp_x',
							   'order' => 'desc',
							   'page_size' => '20',
						  ),
					 )
				);
				CUserOptions::SetOptionsFromArray($arOptions);
				
				/* detail view */
				
					$tabs = array(
					 array(
						  'CODE' => 'edit1',
						  'TITLE' => GetMessage("IBLOCK_ELEMENT_NAME"),
						  'FIELDS' => array(
							   array(
									'NAME' => 'NAME',
									'TITLE' => GetMessage("FORM_NAME"),
							   ),
								array(
								   'NAME' => 'PREVIEW_PICTURE',
								   'TITLE' => GetMessage("FORM_PREVIEW_PICTURE"),
							   ),
							   array(
								   'NAME' => 'PREVIEW_TEXT',
								   'TITLE' => GetMessage("FORM_PREVIEW_TEXT"),
							   ),
								array(
								   'NAME' => "PROPERTY_$arPropsIds[REF]",
								   'TITLE' => GetMessage("FORM_PROPERTY_REF"),
							   ),
							   array(
								   'NAME' => 'IBLOCK_ELEMENT_PROP_VALUE',
								   'TITLE' => GetMessage('IBLOCK_ELEMENT_PROP_VALUE'),
							   ),
							   array(
								   'NAME' => "PROPERTY_$arPropsIds[PAGE]",
								   'TITLE' =>  GetMessage('FORM_ADRESS_WITH_BANNER'),
							   ),
							   array(
								   'NAME' => "PROPERTY_$arPropsIds[SUBSECTION]",
								   'TITLE' => GetMessage('FORM_SHOW_IN_SUBSECTIONS'),
							   ),
							   array(
								   'NAME' => "PROPERTY_$arPropsIds[PAGE_EXCLUSION]",
								   'TITLE' => GetMessage('FORM_PAGE_EXCLUSION'),
							   ),
						   ),
					   ),
					   array(
						   'CODE' => 'edit2',
						   'TITLE' => GetMessage("IBLOCK_SECTION_NAME"),
						   'FIELDS' => array(
							   array(
								   'NAME' => 'SECTIONS',
								   'TITLE' => GetMessage("IBLOCK_SECTION_NAME"),
							   ),
							),
					  ),
				);

					$tabs_string = '';
					foreach($tabs AS $tab)
					{
						$tabs_string .= $tab['CODE'] . '--#--' . $tab['TITLE'] . '--,--';
						foreach($tab['FIELDS'] AS $field)
						{
							$tabs_string .= $field['NAME'] . '--#--' . $field['TITLE'] . '--,--';
						}
						$tabs_string .= "--;--";
					}
					$arOptions = array(
						array(
							'c' => 'form',
							'n' => 'form_element_' . $iblockID,
							'd' => 'Y',
							'v' => array(
								'tabs' => $tabs_string,
							),
						)
					);
					CUserOptions::SetOptionsFromArray($arOptions);
				}
			} else return false;
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CLondonSmartbanner', 'OnBuildGlobalMenu');
		if (cModule::IncludeModule("iblock")){
			CIBlockType::Delete($this->IBLOCK_TYPE);
		}
		else return false;
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
						continue;
					file_put_contents($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item,
					'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.self::MODULE_ID.'/admin/'.$item.'");?'.'>');
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles($p.'/'.$item, $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/'.$item, $ReWrite = True, $Recursive = True);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function UnInstallFiles()
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item);
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || !is_dir($p0 = $p.'/'.$item))
						continue;

					$dir0 = opendir($p0);
					while (false !== $item0 = readdir($dir0))
					{
						if ($item0 == '..' || $item0 == '.')
							continue;
						DeleteDirFilesEx('/bitrix/components/'.$item.'/'.$item0);
					}
					closedir($dir0);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
?>
