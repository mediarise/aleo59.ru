&copy; 2014 Интернет-магазин &quot;E-market&quot;
<a title="Решение разработано студей Крайт" href="http://krayt.ru">Решение разработано студей Крайт</a>
<div class="sale-baner">
    <div>
        <div class="b_title">Готовое решение <br/>"Интернет-магазин элетроники и бытовой техники"</div>
        <div class="b_price">БЕСПЛАТНО</div>
        <div style="text-align: center;">
            <span>Узнать подробнее по телефону:</span>
        </div>
        <div style="text-align: center;" class="b_quest">
            <span >
                8 (800) 555-13-01
            </span>
        </div>
    </div>
    
</div>
<style type="text/css">
    .sale-baner{
        position: fixed;
    left: 0;
    top: 42px;
    background-color: #333333;
    color: #fff;
    z-index: 10000;
    }
    .sale-baner > div {
        position: fixed;
        left: 0;
        top: 42px;
        background-color: #333333;
        color: #fff;
        z-index: 10000;
        border-radius: 3px;
        box-shadow: 0 1px 0 0 rgba(255, 255, 255, 0.1) inset, 2px 2px 2px 0 rgba(0,0,0,0.4);
    }
    .b_title{            
            
            background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, #ff9c00), color-stop(1, #ff6700) );
            background-image: -o-linear-gradient(bottom, #ff9c00 0%, #ff6700 100%);
            background-image: -moz-linear-gradient(bottom, #ff9c00 0%, #ff6700 100%);
            background-image: -webkit-linear-gradient(bottom, #ff9c00 0%, #ff6700 100%);
            background-image: -ms-linear-gradient(bottom, #ff9c00 0%, #ff6700 100%);
            background-image: linear-gradient(to bottom, #ff9c00 0%, #ff6700 100%);
            color: #fff;
            font-size: 16px;    
            text-align: center;        
            padding:10px;
    }
    .b_price{
            font-size: 24px;
            text-align: center;
            padding: 20px 10px;
    }
    .b_quest{
        font-size: 16px;
            text-align: center;
            padding: 10px 10px;
    }
    .btn_pay{
            border-radius: 3px;
            box-shadow: 0 1px 0 0 rgba(255, 255, 255, 0.1) inset, 2px 2px 2px 0 rgba(0,0,0,0.4);
            background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, #3eb900), color-stop(1, #339900) );
            background-image: -o-linear-gradient(bottom, #3eb900 0%, #339900 100%);
            background-image: -moz-linear-gradient(bottom, #3eb900 0%, #339900 100%);
            background-image: -webkit-linear-gradient(bottom, #3eb900 0%, #339900 100%);
            background-image: -ms-linear-gradient(bottom, #3eb900 0%, #339900 100%);
            background-image: linear-gradient(to bottom, #3eb900 0%, #339900 100%);
            color: #fff;
            padding: 10px;
            width: 240px;
            display: inline-block;
            box-sizing: border-box;
            margin: 0 10px;
            text-decoration: none;
            text-align: center;
            font-size: 24px
    }
    .btn_pay:hover{
        background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, #39ab00), color-stop(1, #2b8200) );
        background-image: -o-linear-gradient(bottom, #39ab00 0%, #2b8200 100%);
        background-image: -moz-linear-gradient(bottom, #39ab00 0%, #2b8200 100%);
        background-image: -webkit-linear-gradient(bottom, #39ab00 0%, #2b8200 100%);
        background-image: -ms-linear-gradient(bottom, #39ab00 0%, #2b8200 100%);
        background-image: linear-gradient(to bottom, #39ab00 0%, #2b8200 100%);
    }
    @media screen and (max-width: 500px) and (min-width: 320px){
        .sale-baner{
            display:none;
        }
    }
</style>