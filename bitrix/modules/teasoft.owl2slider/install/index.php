<?
use
		Bitrix\Main\Application,
		Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Class teasoft_owl2slider extends CModule
{
	var $MODULE_ID = 'teasoft.owl2slider';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage($this->MODULE_ID . "_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage($this->MODULE_ID . "_MODULE_DESC");

		$this->PARTNER_NAME = "Evgeniy.Taburyanskiy";//Loc::getMessage($this->MODULE_ID . "_PARTNER_NAME");
		$this->PARTNER_URI = "отсутствует";//Loc::getMessage($this->MODULE_ID . "_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		//RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CTeasoftOwlSlider', 'OnBuildGlobalMenu');
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		//UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CTeasoftOwlSlider', 'OnBuildGlobalMenu');
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		$documentRoot = Application::getDocumentRoot();

		if (is_dir($p = $documentRoot . '/bitrix/modules/' . $this->MODULE_ID . '/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles($p . '/' . $item, $documentRoot . '/bitrix/components/' . $item, $ReWrite = true, $Recursive = true);
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $documentRoot . '/bitrix/modules/' . $this->MODULE_ID . '/install/js'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles(
							$p . '/' . $item, $documentRoot . '/bitrix/js/' . $this->MODULE_ID . '/' . $item,
							$ReWrite = true,
							$Recursive = true);
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $documentRoot . '/bitrix/modules/' . $this->MODULE_ID . '/install/css'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles(
							$p . '/' . $item, $documentRoot . '/bitrix/css/' . $this->MODULE_ID . '/' . $item,
							$ReWrite = true,
							$Recursive = true);
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $documentRoot . '/bitrix/modules/' . $this->MODULE_ID . '/install/images'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles(
							$p . '/' . $item, $documentRoot . '/bitrix/images/' . $this->MODULE_ID . '/' . $item,
							$ReWrite = true,
							$Recursive = true);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFilesEx('/bitrix/js/' . $this->MODULE_ID);
		DeleteDirFilesEx('/bitrix/css/' . $this->MODULE_ID);
		DeleteDirFilesEx('/bitrix/images/' . $this->MODULE_ID);
		DeleteDirFilesEx('/bitrix/components/teasoft/owl2slider');
//		DeleteDirFilesEx('/bitrix/modules/' . $this->MODULE_ID);
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule($this->MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule($this->MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}

?>

