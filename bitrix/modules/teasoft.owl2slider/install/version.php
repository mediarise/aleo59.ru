<?
/**
 *  =====================================================
 *        Company developer: TeaSoft.
 *        Developer: Evgeniy Taburyanskiy
 *        Site: http://
 *        E-mail: Evgeniy.Taburyanskiy@gmail.com
 *        Copyright (c) 2014-2016 TeaSoft
 *  =====================================================
 *        TeaSoft BX modules
 *        13.10.16 9:52
 *  =====================================================
 *
 */

$arModuleVersion = array(
	"VERSION" => "2.1.2",
	"VERSION_DATE" => "2016-11-05 00:00:00"
);
?>