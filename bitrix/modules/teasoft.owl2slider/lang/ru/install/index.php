<?
$MODULE_ID = 'teasoft.owl2slider';

$MESS[$MODULE_ID . "_MODULE_NAME"] = "Слайдер Owl Carousel 2";
$MESS[$MODULE_ID . "_MODULE_DESC"] = "
Новый Адаптивный слайдер на основе популярного плагина Owl Carousel 2.
Работает с данными из модуля Реклама и Инфоблоков.
Множество встроенных анимаций смены слайдов.
Использует аппаратное  ускорение, CSS3 Translate3d переходы.
Встроенная поддержка Touch and Drag
";
$MESS[$MODULE_ID . "_PARTNER_NAME"] = "Evgeniy.Taburyanskiy";
$MESS[$MODULE_ID . "PARTNER_URI"] = "отсутствует";
?>