<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arCurrentValues */

$site = ($_REQUEST["site"] <> '' ? $_REQUEST["site"] : ($_REQUEST["src_site"] <> '' ? $_REQUEST["src_site"] : false));
$arMenu = GetMenuTypes($site);

use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

$arComponentParameters = array(
	"GROUPS" => array(
		"CACHE_SETTINGS" => array(
			"NAME" => Loc::getMessage("COMP_GROUP_CACHE_SETTINGS"),
			"SORT" => 600
		),
		"GROUP_EXT_SETTING" => array(
			"NAME" => Loc::getMessage("GROUP_EXT_SETTING_LOC"),
			"SORT" => 800
		)
	),
	"PARAMETERS" => array(
		"ROOT_MENU_TYPE" => array(
			"NAME" => Loc::getMessage("MAIN_MENU_TYPE_NAME"),
			"TYPE" => "LIST",
			"DEFAULT"=>'left',
			"VALUES" => $arMenu,
			"ADDITIONAL_VALUES"	=> "Y",
			"PARENT" => "BASE",
			"COLS" => 45
		),
		"MAX_LEVEL" => array(
			"NAME" => Loc::getMessage("MAX_LEVEL_NAME"),
			"TYPE" => "LIST",
			"DEFAULT" => '1',
			"PARENT" => "ADDITIONAL_SETTINGS",
			"VALUES" => Array(
				1 => "1",
				2 => "2",
				3 => "3",
				4 => "4",
			),
			"ADDITIONAL_VALUES"	=> "N",
		),
		"CHILD_MENU_TYPE" => Array(
			"NAME" => Loc::getMessage("CHILD_MENU_TYPE_NAME"),
			"TYPE" => "LIST",
			"DEFAULT" => 'left',
			"VALUES" => $arMenu,
			"ADDITIONAL_VALUES"	=> "Y",
			"PARENT" => "ADDITIONAL_SETTINGS",
			"COLS" => 45
		),
		"USE_EXT" => array(
			"NAME" => Loc::getMessage("USE_EXT_NAME"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'N',
			"PARENT" => "ADDITIONAL_SETTINGS",
		),
		"USE_ROUT_EXT" => array(
			"NAME" => Loc::getMessage("USE_ROUT_EXT"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'N',
			"PARENT" => "ADDITIONAL_SETTINGS",
			"REFRESH" => "Y",
		),
		"DELAY" => array(
			"NAME" => Loc::getMessage("DELAY_NAME"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'N',
			"PARENT" => "ADDITIONAL_SETTINGS",
		),
		"ALLOW_MULTI_SELECT" => array(
			"NAME" => Loc::getMessage("comp_menu_allow_multi_select"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'N',
			"PARENT" => "ADDITIONAL_SETTINGS",
		),
		"MENU_CACHE_TYPE" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => Loc::getMessage("COMP_PROP_CACHE_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"A" => GetMessage("COMP_PROP_CACHE_TYPE_AUTO"),
				"Y" => GetMessage("COMP_PROP_CACHE_TYPE_YES"),
				"N" => GetMessage("COMP_PROP_CACHE_TYPE_NO"),
			),
			"DEFAULT" => "N",
			"ADDITIONAL_VALUES" => "N",
		),
		"MENU_CACHE_TIME" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => Loc::getMessage("COMP_PROP_CACHE_TIME"),
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => 3600,
			"COLS" => 5,
		),
		"MENU_CACHE_USE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => Loc::getMessage("CP_BM_MENU_CACHE_USE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
		"MENU_CACHE_GET_VARS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => Loc::getMessage("CP_BM_MENU_CACHE_GET_VARS"),
			"TYPE" => "STRING",
			"MULTIPLE" => "Y",
			"DEFAULT" => "",
			"COLS" => 15,
		),
	)
);

if($arCurrentValues["USE_ROUT_EXT"] == 'Y' && \Bitrix\Main\Loader::includeModule('iblock'))
{
	$arTypesEx = CIBlockParameters::GetIBlockTypes(array("all" => " "));
	$arIBlocks = array();
	$db_iblock = CIBlock::GetList(Array("SORT" => "ASC"), Array("SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"][0] != "all" ? $arCurrentValues["IBLOCK_TYPE"] : "")));
	while($arRes = $db_iblock->Fetch())
		$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

	$arComponentParameters['PARAMETERS'] += array(
		"ELEMENT_ID" => array(
			"PARENT" => "GROUP_EXT_SETTING",
			"NAME" => Loc::getMessage("CP_BMS_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["ELEMENT_ID"]}',
		),
		"IBLOCK_TYPE" => array(
			"PARENT" => "GROUP_EXT_SETTING",
			"NAME" => Loc::getMessage("CP_BMS_IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "catalog",
			"ADDITIONAL_VALUES" => "N",
			"REFRESH" => "Y",
			"MULTIPLE" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "GROUP_EXT_SETTING",
			"NAME" => Loc::getMessage("CP_BMS_IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '1',
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "N",
			"REFRESH" => "Y",
		),
		"DEPTH_LEVEL" => array(
			"PARENT" => "GROUP_EXT_SETTING",
			"NAME" => Loc::getMessage("CP_BMS_DEPTH_LEVEL"),
			"TYPE" => "STRING",
			"DEFAULT" => "1",
		),
		"ONLY_SECTIONS" => array(
			"PARENT" => "GROUP_EXT_SETTING",
			"NAME" => Loc::getMessage("CP_BMS_ONLY_SECTIONS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "N",
		),
	);

	if(count($arCurrentValues["IBLOCK_TYPE"]) > 0 && $arCurrentValues["IBLOCK_TYPE"][0] != 'all')
	{
		$arCategories = array();
		foreach($arCurrentValues["IBLOCK_TYPE"] as $val)
		{
			if(array_key_exists($val, $arTypesEx))
				$arCategories[] = Array(
					"TITLE" => $arTypesEx[$val],
					"KEY" => $val
				);
		}
		foreach($arCategories as $k => $i)
		{
			$arComponentParameters["GROUPS"]["CATEGORY_".$i["KEY"]] = array(
				"NAME" => GetMessage("CP_BST_NUM_CATEGORY", array("#TITLE#" => $arCategories[$k]["TITLE"])),
				"SORT" => 900
			);
			$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_LINK"] = array(
				"PARENT" => "CATEGORY_".$i["KEY"],
				"NAME" => GetMessage("CP_BST_CATEGORY_TITLE").' ('.GetMessage("CP_BMS_SEF_BASE_URL").')',
				"TYPE" => "STRING",
			);
			$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_IS_SEF"] = array(
				"PARENT" => "CATEGORY_".$i["KEY"],
				"NAME" => GetMessage("CP_BMS_IS_SEF"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			);
			if($arCurrentValues["CATEGORY_".$i["KEY"]."_IS_SEF"] == 'Y') {
				$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_SECTION_LINK"] = CIBlockParameters::GetPathTemplateParam(
					"SECTION",
					"SECTION_PAGE_URL",
					GetMessage("CP_BMS_SECTION_PAGE_URL"),
					"#SECTION_CODE#/",
					"CATEGORY_".$i["KEY"]
				);
				$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_DETAIL_LINK"] = CIBlockParameters::GetPathTemplateParam(
					"DETAIL",
					"DETAIL_PAGE_URL",
					GetMessage("CP_BMS_DETAIL_PAGE_URL"),
					"#SECTION_CODE#/#ELEMENT_ID#/",
					"CATEGORY_".$i["KEY"]
				);
			}
		}
	}
	if((count($arCurrentValues["IBLOCK_ID"]) > 0) && (count($arCurrentValues["IBLOCK_TYPE"]) <= 0 || $arCurrentValues["IBLOCK_TYPE"][0] == 'all'))
	{
		$arCategories = Array();
		foreach($arCurrentValues["IBLOCK_ID"] as $val)
		{
			if(array_key_exists($val, $arIBlocks))
				$arCategories[] = Array(
					"TITLE" => $arIBlocks[$val],
					"KEY" => $val
				);
		}
		foreach($arCategories as $k => $i)
		{
			$arComponentParameters["GROUPS"]["CATEGORY_".$i["KEY"]] = array(
				"NAME" => GetMessage("CP_BST_NUM_CATEGORY", array("#TITLE#" => $arCategories[$k]["TITLE"])),
				"SORT" => 900
			);
			$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_IS_SEF"] = array(
				"PARENT" => "CATEGORY_".$i["KEY"],
				"NAME" => GetMessage("CP_BMS_IS_SEF"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "N",
				"REFRESH" => "Y",
			);
			if($arCurrentValues["CATEGORY_".$i["KEY"]."_IS_SEF"] == 'Y') {
				$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_LINK"] = array(
					"PARENT" => "CATEGORY_".$i["KEY"],
					"NAME" => GetMessage("CP_BMS_SEF_BASE_URL"),
					"TYPE" => "STRING",
				);
				$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_SECTION_LINK"] = CIBlockParameters::GetPathTemplateParam(
					"SECTION",
					"SECTION_PAGE_URL",
					GetMessage("CP_BMS_SECTION_PAGE_URL"),
					"#SECTION_CODE#/",
					"CATEGORY_".$i["KEY"]
				);
				$arComponentParameters["PARAMETERS"]["CATEGORY_".$i["KEY"]."_DETAIL_LINK"] = CIBlockParameters::GetPathTemplateParam(
					"DETAIL",
					"DETAIL_PAGE_URL",
					GetMessage("CP_BMS_DETAIL_PAGE_URL"),
					"#SECTION_CODE#/#ELEMENT_ID#/",
					"CATEGORY_".$i["KEY"]
				);
			}
		}
	}
}