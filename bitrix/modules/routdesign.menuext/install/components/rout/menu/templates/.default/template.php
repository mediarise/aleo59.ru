<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if(!empty($arResult)): ?>
    <div class="top_menu">
		<ul class="menu" data-role="x-menu">
			<?
				$previousLevel = 0;
				foreach($arResult as $arItem):
			?>
				<? if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
					<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
				<? endif; ?>
				<? if($arItem["IS_PARENT"]): ?>
					<? if($arItem["DEPTH_LEVEL"] == 1): ?>
						<li class="first_lvl_parent parent_li<? if($arItem["SELECTED"]): ?> first_lvl_selected<? endif; ?>"><span class="parent_span"><a href="<?=$arItem["LINK"]?>" class="<? if($arItem["SELECTED"]): ?>root-item-selected<? else: ?>root-item<? endif; ?>"><?=$arItem["TEXT"]?></a></span>
							<ul class="first_lvl">
					<? else: ?>
						<li class="parent_li<? if($arItem["SELECTED"]): ?> item-selected<? endif; ?>">
							<span class="parent_span">
								<a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
								<i class="l_top"></i><i class="l_bt"></i>
							</span>
							<ul class="parent_lvl">
								<li class="zap_top"></li>
								<li class="zap_bt"></li>
					<? endif; ?>
				<? else: ?>
					<? if($arItem["PERMISSION"] > "D"): ?>
						<? if($arItem["DEPTH_LEVEL"] == 1): ?>
							<li class="not_parent<? if($arItem["SELECTED"]): ?> first_lvl_selected<? endif; ?>"><span><a href="<?=$arItem["LINK"]?>" class="<? if($arItem["SELECTED"]): ?>root-item-selected<? else: ?>root-item<? endif; ?>"><?=$arItem["TEXT"]?></a></span></li>
						<? else: ?>
							<li<? if($arItem["SELECTED"]): ?> class="item-selected"<? endif; ?>><span><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></span></li>
						<? endif; ?>
					<? endif; ?>
				<? endif; ?>
				<? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
			<? endforeach; ?>
			<? if($previousLevel > 1): ?>
				<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
			<? endif; ?>
		</ul>
	</div>
<? endif; ?>