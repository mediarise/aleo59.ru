<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/" . SITE_TEMPLATE_ID . "/header.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
?>
<!DOCTYPE html>
<html xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_DIR ?>favicon.ico"/>
    <? $APPLICATION->ShowHead(); ?>
    <?
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/colors.css", true);
    $APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
    $APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
    ?>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <title><? $APPLICATION->ShowTitle() ?></title>

    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
</head>
<body class="bx-background-image bx-theme-<?= $theme ?>" <?= $APPLICATION->ShowProperty("backgroundImage") ?>>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? $APPLICATION->IncludeComponent("bitrix:eshop.banner", "", array()); ?>
<div class="bx-wrapper" id="bx_eshop_wrap">
    <header class="bx-header">


        <div class="mr-top-menu">
            <div class="container">
                <div class="row">

                    <div class="col-sm-5 hidden-md"></div>
                    <div class="col-sm-7 col-md-12">

                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "mr_top_menu",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "3",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_THEME" => "site",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "N",
   "IBLOCK_TYPE" => "1c_catalog",
    "IBLOCK_ID" => "8",
                            )
                        ); ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="mr-header-wrap">
            <div class="bx-header-section container">


                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="bx-logo" style="text-align: center;">
                            <a class="bx-logo-block hidden-xs" href="<?= SITE_DIR ?>">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/company_logo.php"), false); ?>
                            </a>
                            <a class="bx-logo-block hidden-lg hidden-md hidden-sm text-center" href="<?= SITE_DIR ?>">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/company_logo_mobile.php"), false); ?>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                        <div class="bx-worktime">
                            <div class="bx-worktime-prop">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/contact.php"), false); ?>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-right">
                        <div class="bx-inc-orginfo">
                            <div>
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/telephone.php"), false); ?>
                            </div>
                            <div>
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/schedule.php"), false); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 hidden-xs" style="padding-top: 10px;">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/company_logo_kompleks.php"), false); ?>


                    </div>



                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 hidden-xs">
                <? $APPLICATION->IncludeComponent("bitrix:menu", "mr_main_menu", array(
                    "ROOT_MENU_TYPE" => "left",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_THEME" => "site",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "IBLOCK_ID" => "8",
                    "IBLOCK_TYPE" => "1c_catalog",
                ),
                    false
                ); ?>
            </div>
        </div>
        <? if ($curPage != SITE_DIR . "index.php"): ?>
            <div class="bx-header-section container">

                <div class="row">
                    <div class="col-lg-12" id="navigation">
                        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "-"
                        ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        ); ?>
                    </div>
                </div>
                <h1 class="bx-title dbg_title" id="pagetitle"><?= $APPLICATION->ShowTitle(false); ?></h1>

            </div>
        <? endif ?>
</div>
</header>





<? $needSidebar = preg_match("~^" . SITE_DIR . "(catalog|personal\/cart|personal\/order\/make)/~", $curPage); ?>

<?php if ($APPLICATION->GetCurPage(false) === '/'): ?>


<?php else: ?>
<div class="workarea">
    <div class="container bx-content-seection">
        <div class="row">

    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "sect",
            "AREA_FILE_SUFFIX" => "bottom",
            "AREA_FILE_RECURSIVE" => "N",
            "EDIT_MODE" => "html",
        ),
        false,
        Array('HIDE_ICONS' => 'Y')
    );?>
    
            <div class="bx-content <?= ($needSidebar ? "col-xs-12" : "col-md-9 col-sm-8") ?>">
                <?php endif; ?>